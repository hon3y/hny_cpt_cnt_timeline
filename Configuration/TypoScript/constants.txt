
plugin.tx_teufelscptcnttimeline_teufelstimeline {
	view {
		# cat=plugin.tx_teufelscptcnttimeline_teufelstimeline/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:teufels_cpt_cnt_timeline/Resources/Private/Templates/
		# cat=plugin.tx_teufelscptcnttimeline_teufelstimeline/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:teufels_cpt_cnt_timeline/Resources/Private/Partials/
		# cat=plugin.tx_teufelscptcnttimeline_teufelstimeline/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:teufels_cpt_cnt_timeline/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_teufelscptcnttimeline_teufelstimeline//a; type=string; label=Default storage PID
		storagePid =
	}
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
plugin.tx_teufelscptcnttimeline_teufelstimeline {
    settings {
        production {
            includePath {
                public = EXT:teufels_cpt_cnt_timeline/Resources/Public/
                private = EXT:teufels_cpt_cnt_timeline/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_cpt_cnt_timeline/Resources/Public/
                }
            }
        }
    }
}
plugin.tx_teufelscptcnttimeline_teufelstimeline.persistence.storagePid = 81