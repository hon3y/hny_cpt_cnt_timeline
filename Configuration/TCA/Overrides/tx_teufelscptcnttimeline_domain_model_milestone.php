<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_teufelscptcnttimeline_domain_model_milestone']['types']['1']['showitem'] = '--div--;Milestone,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, date, title, subtitle, description;;;richtext:rte_transform[mode=ts_links], image, 
--div--;Additional File,file, 
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden;;1, starttime, endtime';


$GLOBALS['TCA']['tx_teufelscptcnttimeline_domain_model_milestone']['ctrl']['label'] = 'date';
$GLOBALS['TCA']['tx_teufelscptcnttimeline_domain_model_milestone']['ctrl']['label_alt'] = 'title';
$GLOBALS['TCA']['tx_teufelscptcnttimeline_domain_model_milestone']['ctrl']['label_alt_force'] =  1;